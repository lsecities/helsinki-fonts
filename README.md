Helsinki web fonts for LSE Cities web projects
==============================================

As these fonts are distributed under a proprietary license, we can't just check
them in with the main WordPress theme assets: therefore we use this Bower
package, which we then bring into the installed theme by running bower
install/update.
